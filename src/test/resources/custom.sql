CREATE TABLE `system_config` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Client_Type` tinyint(1) DEFAULT '0' COMMENT '类型 0：通用',
  `Group_Id` bigint(20) DEFAULT '0' COMMENT '组ID',
  `Item_Type` tinyint(1) DEFAULT '0' COMMENT '配置类型',
  `Type_Name` varchar(10) DEFAULT '' COMMENT '类型名称',
  `Item_Key` varchar(100) DEFAULT '' COMMENT '配置项',
  `Item_Value` varchar(255) DEFAULT '' COMMENT '配置值',
  `Comment` varchar(255) DEFAULT '' COMMENT '说明',
  `Creator` varchar(32) DEFAULT '' COMMENT '创建者',
  `Is_Delete` tinyint(1) DEFAULT '0' COMMENT '删除状态\r\n            0：未删除\r\n            1：已删除',
  `Create_Time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `Last_Time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后更新时间',
  PRIMARY KEY (`Id`),
  KEY `idx_syc_GroupId` (`Group_Id`) USING BTREE,
  KEY `idx_syc_ItemType` (`Item_Type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='系统全局配置表';


CREATE TABLE `account` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Account` varchar(32) DEFAULT '' COMMENT '登录帐号(手机号)',
  `Password` varchar(64) DEFAULT '' COMMENT '密码',
  `User_Type` tinyint(1) DEFAULT '0' COMMENT '用户类型 0:普通用户 1：管理员',
  `Status` tinyint(1) DEFAULT '0' COMMENT '用户状态  0：正常 1：锁定 2：冻结 ',
  `Real_Name` varchar(64) DEFAULT '' COMMENT '姓名',
  `nick_name` varchar(64) DEFAULT '' COMMENT '昵称',
  `Balance` decimal(10,2) DEFAULT '0.00' COMMENT '余额',
  `Withdraw_Balance` decimal(10,0) DEFAULT '0' COMMENT '可提现余额',
  `Access_Token` varchar(64) DEFAULT '' COMMENT '访问token',
  `Is_Delete` tinyint(1) DEFAULT '0' COMMENT '删除状态 0：未删除 1：已删除',
  `Email` varchar(64) DEFAULT '' COMMENT 'Email',
  `mail_Check` tinyint(1) DEFAULT '0' COMMENT '邮箱是否已验证 0:未验证1:已验证',
  `signature` varchar(160) DEFAULT '' COMMENT '个性签名',
  `Sex` tinyint(1) DEFAULT '1' COMMENT '性别 1:男,2:女',
  `Age` tinyint(1) DEFAULT '18' COMMENT '年龄',
  `Mobile_Check` tinyint(1) DEFAULT '0' COMMENT '手机验证状态',
  `IMU_id` varchar(64) DEFAULT '' COMMENT 'IM身份ID',
  `Icon_Photo_Id` varchar(64) DEFAULT '' COMMENT '图像文件ID',
  `Merchant_Id` varchar(64) DEFAULT '' COMMENT '第三方商户ID',
  `weixin_UnionId` varchar(64) DEFAULT '',
  `weixin_OpenId` varchar(64) DEFAULT '' COMMENT 'app',
  `weixinWeb_OpenId` varchar(64) DEFAULT '' COMMENT 'web',
  `qq_OpenId` varchar(64) DEFAULT '',
  `qq_UnionId` varchar(64) DEFAULT '',
  `location` varchar(150) DEFAULT '' COMMENT '地址',
  `experience` int(11) DEFAULT '0' COMMENT '经验值',
  `vip_Level` int(11) DEFAULT '0' COMMENT 'VIP等级',
  `Version` int(11) DEFAULT '0' COMMENT '乐观锁版本控制',
  `Reg_Platform` tinyint(1) DEFAULT '0' COMMENT '注册使用平台  0：UNKNOWN  1：ANDROID  2：IOS',
  `Reg_IMEI` varchar(64) DEFAULT '' COMMENT '注册设备标识',
  `Last_Platform` tinyint(1) DEFAULT '0' COMMENT '最近使用平台 0：UNKNOWN 1：ANDROID 2：IOS',
  `Last_IMEI` varchar(64) DEFAULT '' COMMENT '设备标识',
  `Last_Login` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '最后登录时间',
  `Last_Login_Type` tinyint(1) DEFAULT '0' COMMENT '最后登录方式  0：未知 1：密码登录 2：验证码登录  3：免密登录 4：其它',
  `Login_Times` int(11) DEFAULT '0' COMMENT '累积登录次数',
  `Inviter_Account_Id` bigint(20) DEFAULT '0' COMMENT '邀请者ID',
  `Invit_Time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '邀请绑定时间',
  `Invit_Type` tinyint(4) DEFAULT '0' COMMENT '邀请类型:0:用户正常邀请, 1:系统绑定',
  `Is_Real_Name_Auth` tinyint(1) DEFAULT '0' COMMENT '是否已经实名认证',
  `Last_Change_Time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '最后更换账户时间',
  `ip_address` varchar(20) DEFAULT '' COMMENT 'ip地址',
  `ip_location` varchar(20) DEFAULT '' COMMENT 'ip归属地',
  `comment` varchar(64) DEFAULT '' COMMENT '备注',
  `Create_Time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `Last_Time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后更新时间',
  PRIMARY KEY (`Id`),
  KEY `idx_Account` (`Account`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='帐号表';


CREATE TABLE `app_version` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Platform` tinyint(1) DEFAULT '1' COMMENT '平台类型\r\n            1：ANDROID\r\n            2：IOS',
  `Version` varchar(10) DEFAULT '' COMMENT '版本号',
  `Min_Version` varchar(10) DEFAULT '' COMMENT '最低版本以下要求强制更新',
  `Update_Type` tinyint(1) DEFAULT '1' COMMENT '升级方式\r\n            1：提示升级\r\n            2：强制升级\r\n            3：提示转向下载\r\n            4：强制转向下载',
  `URL` varchar(255) DEFAULT '' COMMENT '下载地址',
  `Update_Content` varchar(255) DEFAULT '' COMMENT '更新内容(回车符用"</br>"标记)',
  `Size` bigint(20) NOT NULL DEFAULT '0' COMMENT '文件大小(字节)',
  `Build_Version` int(11) DEFAULT '0' COMMENT 'build版本号,用于前台进行版本号对比',
  `Create_Time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `Last_Time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后更新时间',
  `Client_Type` tinyint(1) DEFAULT '0' COMMENT '客户端类型',
  `Is_Delete` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='版本管理配置';

CREATE TABLE `login_log` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Account_Id` bigint(20) DEFAULT '0' COMMENT '用户ID',
  `Platform` tinyint(1) DEFAULT '0' COMMENT '平台\r\n            0：UNKNOWN\r\n            1：ANDROID\r\n            2：IOS',
  `IMEI` varchar(64) DEFAULT '' COMMENT '设置标识',
  `Login_Type` tinyint(1) DEFAULT '0' COMMENT '登录方式\r\n            0：未知\r\n            1：密码登录\r\n            2：验证码登录\r\n            3：免密登录\r\n            4：其它',
  `Login_Time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '登录时间',
  `Create_Time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户登录日志';


CREATE TABLE `merchant_info` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Merchant_Id` varchar(64) DEFAULT '' COMMENT '第三方商户',
  `Merchant_Name` varchar(64) DEFAULT '' COMMENT '商户名称',
  `Public_Key` varchar(255) DEFAULT '' COMMENT '公钥',
  `Private_Key` varchar(255) DEFAULT '' COMMENT '私钥',
  `Is_Delete` tinyint(1) DEFAULT '0' COMMENT '删除状态\r\n            0：未删除\r\n            1：已删除',
  `Create_Time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `Last_Time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后更新时间',
  PRIMARY KEY (`Id`),
  KEY `idx_McInfo_CrTime` (`Create_Time`),
  KEY `idx_McInfo_MId` (`Merchant_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='第三方商户信息';

CREATE TABLE `sms_history` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Channel` varchar(64) DEFAULT '' COMMENT '通道名称',
  `Source` varchar(64) DEFAULT '' COMMENT '请求来源',
  `Content` varchar(255) DEFAULT '' COMMENT '发送内容',
  `State` tinyint(1) DEFAULT '1' COMMENT '发送状态\r\n            1：成功\r\n            0：失败',
  `Phone` varchar(32) DEFAULT '' COMMENT '电话号码',
  `Create_Time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `Last_Time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '最后更新时间',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='短信发送历史';


CREATE TABLE `upload_files_log` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `File_Type` int(11) DEFAULT '-1' COMMENT '文件用途类型',
  `File_Key` varchar(255) DEFAULT '' COMMENT '文件osskey',
  `Owner_Id` bigint(255) DEFAULT '0' COMMENT '上传者',
  `Is_Delete` tinyint(1) DEFAULT '0' COMMENT '删除状态',
  `Create_Time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

