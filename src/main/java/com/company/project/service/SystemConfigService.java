package com.company.project.service;
import com.company.project.model.SystemConfig;
import com.company.project.core.Service;


/**
 * Created by xyz on 2018/04/15.
 */
public interface SystemConfigService extends Service<SystemConfig> {

}
