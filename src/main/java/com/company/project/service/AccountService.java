package com.company.project.service;
import com.company.project.model.Account;
import com.company.project.core.Service;


/**
 * Created by xyz on 2018/04/15.
 */
public interface AccountService extends Service<Account> {

    Account getUserByUsername(String username);

}
