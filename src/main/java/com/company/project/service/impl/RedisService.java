package com.company.project.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class RedisService {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    public void set(String key, String val) {
        if (StringUtils.isEmpty(key)) return;
        stringRedisTemplate.opsForValue().set(key, val);
    }

    public String get(String key) {
        if (StringUtils.isEmpty(key)) return null;
        return stringRedisTemplate.opsForValue().get(key);
    }


}
