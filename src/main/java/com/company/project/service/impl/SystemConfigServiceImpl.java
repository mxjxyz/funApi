package com.company.project.service.impl;

import com.company.project.dao.SystemConfigMapper;
import com.company.project.model.SystemConfig;
import com.company.project.service.SystemConfigService;
import com.company.project.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by xyz on 2018/04/15.
 */
@Service
@Transactional
public class SystemConfigServiceImpl extends AbstractService<SystemConfig> implements SystemConfigService {
    @Resource
    private SystemConfigMapper systemConfigMapper;

}
