package com.company.project.pojo;

import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

public class PoAccount {

//    @NotEmpty
    @ApiModelProperty(value="账号" ,required=true)
    private String account;

//    @NotEmpty
    @ApiModelProperty(value="密码，MD5后的" ,required=true)
    private String password;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
