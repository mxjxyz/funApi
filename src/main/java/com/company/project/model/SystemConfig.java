package com.company.project.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "system_config")
public class SystemConfig implements Serializable {
    @Id
    @Column(name = "Id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 类型 0：通用
     */
    @Column(name = "Client_Type")
    private Boolean clientType;

    /**
     * 组ID
     */
    @Column(name = "Group_Id")
    private Long groupId;

    /**
     * 配置类型
     */
    @Column(name = "Item_Type")
    private Boolean itemType;

    /**
     * 类型名称
     */
    @Column(name = "Type_Name")
    private String typeName;

    /**
     * 配置项
     */
    @Column(name = "Item_Key")
    private String itemKey;

    /**
     * 配置值
     */
    @Column(name = "Item_Value")
    private String itemValue;

    /**
     * 说明
     */
    @Column(name = "Comment")
    private String comment;

    /**
     * 创建者
     */
    @Column(name = "Creator")
    private String creator;

    /**
     * 删除状态
            0：未删除
            1：已删除
     */
    @Column(name = "Is_Delete")
    private Boolean isDelete;

    /**
     * 创建时间
     */
    @Column(name = "Create_Time")
    private Date createTime;

    /**
     * 最后更新时间
     */
    @Column(name = "Last_Time")
    private Date lastTime;

    private static final long serialVersionUID = 1L;

    /**
     * @return Id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取类型 0：通用
     *
     * @return Client_Type - 类型 0：通用
     */
    public Boolean getClientType() {
        return clientType;
    }

    /**
     * 设置类型 0：通用
     *
     * @param clientType 类型 0：通用
     */
    public void setClientType(Boolean clientType) {
        this.clientType = clientType;
    }

    /**
     * 获取组ID
     *
     * @return Group_Id - 组ID
     */
    public Long getGroupId() {
        return groupId;
    }

    /**
     * 设置组ID
     *
     * @param groupId 组ID
     */
    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    /**
     * 获取配置类型
     *
     * @return Item_Type - 配置类型
     */
    public Boolean getItemType() {
        return itemType;
    }

    /**
     * 设置配置类型
     *
     * @param itemType 配置类型
     */
    public void setItemType(Boolean itemType) {
        this.itemType = itemType;
    }

    /**
     * 获取类型名称
     *
     * @return Type_Name - 类型名称
     */
    public String getTypeName() {
        return typeName;
    }

    /**
     * 设置类型名称
     *
     * @param typeName 类型名称
     */
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    /**
     * 获取配置项
     *
     * @return Item_Key - 配置项
     */
    public String getItemKey() {
        return itemKey;
    }

    /**
     * 设置配置项
     *
     * @param itemKey 配置项
     */
    public void setItemKey(String itemKey) {
        this.itemKey = itemKey;
    }

    /**
     * 获取配置值
     *
     * @return Item_Value - 配置值
     */
    public String getItemValue() {
        return itemValue;
    }

    /**
     * 设置配置值
     *
     * @param itemValue 配置值
     */
    public void setItemValue(String itemValue) {
        this.itemValue = itemValue;
    }

    /**
     * 获取说明
     *
     * @return Comment - 说明
     */
    public String getComment() {
        return comment;
    }

    /**
     * 设置说明
     *
     * @param comment 说明
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * 获取创建者
     *
     * @return Creator - 创建者
     */
    public String getCreator() {
        return creator;
    }

    /**
     * 设置创建者
     *
     * @param creator 创建者
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * 获取删除状态
            0：未删除
            1：已删除
     *
     * @return Is_Delete - 删除状态
            0：未删除
            1：已删除
     */
    public Boolean getIsDelete() {
        return isDelete;
    }

    /**
     * 设置删除状态
            0：未删除
            1：已删除
     *
     * @param isDelete 删除状态
            0：未删除
            1：已删除
     */
    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    /**
     * 获取创建时间
     *
     * @return Create_Time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取最后更新时间
     *
     * @return Last_Time - 最后更新时间
     */
    public Date getLastTime() {
        return lastTime;
    }

    /**
     * 设置最后更新时间
     *
     * @param lastTime 最后更新时间
     */
    public void setLastTime(Date lastTime) {
        this.lastTime = lastTime;
    }
}