package com.company.project.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

public class Account implements Serializable {
    @Id
    @Column(name = "Id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 登录帐号(手机号)
     */
    @Column(name = "Account")
    private String account;

    /**
     * 密码
     */
    @Column(name = "Password")
    private String password;

    /**
     * 用户类型 0:普通用户 1：管理员
     */
    @Column(name = "User_Type")
    private Boolean userType;

    /**
     * 用户状态  0：正常 1：锁定 2：冻结 
     */
    @Column(name = "Status")
    private Boolean status;

    /**
     * 姓名
     */
    @Column(name = "Real_Name")
    private String realName;

    /**
     * 昵称
     */
    @Column(name = "nick_name")
    private String nickName;

    /**
     * 余额
     */
    @Column(name = "Balance")
    private BigDecimal balance;

    /**
     * 可提现余额
     */
    @Column(name = "Withdraw_Balance")
    private Long withdrawBalance;

    /**
     * 访问token
     */
    @Column(name = "Access_Token")
    private String accessToken;

    /**
     * 删除状态 0：未删除 1：已删除
     */
    @Column(name = "Is_Delete")
    private Boolean isDelete;

    /**
     * Email
     */
    @Column(name = "Email")
    private String email;

    /**
     * 邮箱是否已验证 0:未验证1:已验证
     */
    @Column(name = "mail_Check")
    private Boolean mailCheck;

    /**
     * 个性签名
     */
    private String signature;

    /**
     * 性别 1:男,2:女
     */
    @Column(name = "Sex")
    private Boolean sex;

    /**
     * 年龄
     */
    @Column(name = "Age")
    private Boolean age;

    /**
     * 手机验证状态
     */
    @Column(name = "Mobile_Check")
    private Boolean mobileCheck;

    /**
     * IM身份ID
     */
    @Column(name = "IMU_id")
    private String imuId;

    /**
     * 图像文件ID
     */
    @Column(name = "Icon_Photo_Id")
    private String iconPhotoId;

    /**
     * 第三方商户ID
     */
    @Column(name = "Merchant_Id")
    private String merchantId;

    @Column(name = "weixin_UnionId")
    private String weixinUnionid;

    /**
     * app
     */
    @Column(name = "weixin_OpenId")
    private String weixinOpenid;

    /**
     * web
     */
    @Column(name = "weixinWeb_OpenId")
    private String weixinwebOpenid;

    @Column(name = "qq_OpenId")
    private String qqOpenid;

    @Column(name = "qq_UnionId")
    private String qqUnionid;

    /**
     * 地址
     */
    private String location;

    /**
     * 经验值
     */
    private Integer experience;

    /**
     * VIP等级
     */
    @Column(name = "vip_Level")
    private Integer vipLevel;

    /**
     * 乐观锁版本控制
     */
    @Column(name = "Version")
    private Integer version;

    /**
     * 注册使用平台  0：UNKNOWN  1：ANDROID  2：IOS
     */
    @Column(name = "Reg_Platform")
    private Boolean regPlatform;

    /**
     * 注册设备标识
     */
    @Column(name = "Reg_IMEI")
    private String regImei;

    /**
     * 最近使用平台 0：UNKNOWN 1：ANDROID 2：IOS
     */
    @Column(name = "Last_Platform")
    private Boolean lastPlatform;

    /**
     * 设备标识
     */
    @Column(name = "Last_IMEI")
    private String lastImei;

    /**
     * 最后登录时间
     */
    @Column(name = "Last_Login")
    private Date lastLogin;

    /**
     * 最后登录方式  0：未知 1：密码登录 2：验证码登录  3：免密登录 4：其它
     */
    @Column(name = "Last_Login_Type")
    private Boolean lastLoginType;

    /**
     * 累积登录次数
     */
    @Column(name = "Login_Times")
    private Integer loginTimes;

    /**
     * 邀请者ID
     */
    @Column(name = "Inviter_Account_Id")
    private Long inviterAccountId;

    /**
     * 邀请绑定时间
     */
    @Column(name = "Invit_Time")
    private Date invitTime;

    /**
     * 邀请类型:0:用户正常邀请, 1:系统绑定
     */
    @Column(name = "Invit_Type")
    private Byte invitType;

    /**
     * 是否已经实名认证
     */
    @Column(name = "Is_Real_Name_Auth")
    private Boolean isRealNameAuth;

    /**
     * 最后更换账户时间
     */
    @Column(name = "Last_Change_Time")
    private Date lastChangeTime;

    /**
     * ip地址
     */
    @Column(name = "ip_address")
    private String ipAddress;

    /**
     * ip归属地
     */
    @Column(name = "ip_location")
    private String ipLocation;

    /**
     * 备注
     */
    private String comment;

    /**
     * 创建时间
     */
    @Column(name = "Create_Time")
    private Date createTime;

    /**
     * 最后更新时间
     */
    @Column(name = "Last_Time")
    private Date lastTime;

    private static final long serialVersionUID = 1L;

    /**
     * @return Id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取登录帐号(手机号)
     *
     * @return Account - 登录帐号(手机号)
     */
    public String getAccount() {
        return account;
    }

    /**
     * 设置登录帐号(手机号)
     *
     * @param account 登录帐号(手机号)
     */
    public void setAccount(String account) {
        this.account = account;
    }

    /**
     * 获取密码
     *
     * @return Password - 密码
     */
    public String getPassword() {
        return password;
    }

    /**
     * 设置密码
     *
     * @param password 密码
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 获取用户类型 0:普通用户 1：管理员
     *
     * @return User_Type - 用户类型 0:普通用户 1：管理员
     */
    public Boolean getUserType() {
        return userType;
    }

    /**
     * 设置用户类型 0:普通用户 1：管理员
     *
     * @param userType 用户类型 0:普通用户 1：管理员
     */
    public void setUserType(Boolean userType) {
        this.userType = userType;
    }

    /**
     * 获取用户状态  0：正常 1：锁定 2：冻结 
     *
     * @return Status - 用户状态  0：正常 1：锁定 2：冻结 
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * 设置用户状态  0：正常 1：锁定 2：冻结 
     *
     * @param status 用户状态  0：正常 1：锁定 2：冻结 
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * 获取姓名
     *
     * @return Real_Name - 姓名
     */
    public String getRealName() {
        return realName;
    }

    /**
     * 设置姓名
     *
     * @param realName 姓名
     */
    public void setRealName(String realName) {
        this.realName = realName;
    }

    /**
     * 获取昵称
     *
     * @return nick_name - 昵称
     */
    public String getNickName() {
        return nickName;
    }

    /**
     * 设置昵称
     *
     * @param nickName 昵称
     */
    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    /**
     * 获取余额
     *
     * @return Balance - 余额
     */
    public BigDecimal getBalance() {
        return balance;
    }

    /**
     * 设置余额
     *
     * @param balance 余额
     */
    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    /**
     * 获取可提现余额
     *
     * @return Withdraw_Balance - 可提现余额
     */
    public Long getWithdrawBalance() {
        return withdrawBalance;
    }

    /**
     * 设置可提现余额
     *
     * @param withdrawBalance 可提现余额
     */
    public void setWithdrawBalance(Long withdrawBalance) {
        this.withdrawBalance = withdrawBalance;
    }

    /**
     * 获取访问token
     *
     * @return Access_Token - 访问token
     */
    public String getAccessToken() {
        return accessToken;
    }

    /**
     * 设置访问token
     *
     * @param accessToken 访问token
     */
    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    /**
     * 获取删除状态 0：未删除 1：已删除
     *
     * @return Is_Delete - 删除状态 0：未删除 1：已删除
     */
    public Boolean getIsDelete() {
        return isDelete;
    }

    /**
     * 设置删除状态 0：未删除 1：已删除
     *
     * @param isDelete 删除状态 0：未删除 1：已删除
     */
    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    /**
     * 获取Email
     *
     * @return Email - Email
     */
    public String getEmail() {
        return email;
    }

    /**
     * 设置Email
     *
     * @param email Email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 获取邮箱是否已验证 0:未验证1:已验证
     *
     * @return mail_Check - 邮箱是否已验证 0:未验证1:已验证
     */
    public Boolean getMailCheck() {
        return mailCheck;
    }

    /**
     * 设置邮箱是否已验证 0:未验证1:已验证
     *
     * @param mailCheck 邮箱是否已验证 0:未验证1:已验证
     */
    public void setMailCheck(Boolean mailCheck) {
        this.mailCheck = mailCheck;
    }

    /**
     * 获取个性签名
     *
     * @return signature - 个性签名
     */
    public String getSignature() {
        return signature;
    }

    /**
     * 设置个性签名
     *
     * @param signature 个性签名
     */
    public void setSignature(String signature) {
        this.signature = signature;
    }

    /**
     * 获取性别 1:男,2:女
     *
     * @return Sex - 性别 1:男,2:女
     */
    public Boolean getSex() {
        return sex;
    }

    /**
     * 设置性别 1:男,2:女
     *
     * @param sex 性别 1:男,2:女
     */
    public void setSex(Boolean sex) {
        this.sex = sex;
    }

    /**
     * 获取年龄
     *
     * @return Age - 年龄
     */
    public Boolean getAge() {
        return age;
    }

    /**
     * 设置年龄
     *
     * @param age 年龄
     */
    public void setAge(Boolean age) {
        this.age = age;
    }

    /**
     * 获取手机验证状态
     *
     * @return Mobile_Check - 手机验证状态
     */
    public Boolean getMobileCheck() {
        return mobileCheck;
    }

    /**
     * 设置手机验证状态
     *
     * @param mobileCheck 手机验证状态
     */
    public void setMobileCheck(Boolean mobileCheck) {
        this.mobileCheck = mobileCheck;
    }

    /**
     * 获取IM身份ID
     *
     * @return IMU_id - IM身份ID
     */
    public String getImuId() {
        return imuId;
    }

    /**
     * 设置IM身份ID
     *
     * @param imuId IM身份ID
     */
    public void setImuId(String imuId) {
        this.imuId = imuId;
    }

    /**
     * 获取图像文件ID
     *
     * @return Icon_Photo_Id - 图像文件ID
     */
    public String getIconPhotoId() {
        return iconPhotoId;
    }

    /**
     * 设置图像文件ID
     *
     * @param iconPhotoId 图像文件ID
     */
    public void setIconPhotoId(String iconPhotoId) {
        this.iconPhotoId = iconPhotoId;
    }

    /**
     * 获取第三方商户ID
     *
     * @return Merchant_Id - 第三方商户ID
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * 设置第三方商户ID
     *
     * @param merchantId 第三方商户ID
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return weixin_UnionId
     */
    public String getWeixinUnionid() {
        return weixinUnionid;
    }

    /**
     * @param weixinUnionid
     */
    public void setWeixinUnionid(String weixinUnionid) {
        this.weixinUnionid = weixinUnionid;
    }

    /**
     * 获取app
     *
     * @return weixin_OpenId - app
     */
    public String getWeixinOpenid() {
        return weixinOpenid;
    }

    /**
     * 设置app
     *
     * @param weixinOpenid app
     */
    public void setWeixinOpenid(String weixinOpenid) {
        this.weixinOpenid = weixinOpenid;
    }

    /**
     * 获取web
     *
     * @return weixinWeb_OpenId - web
     */
    public String getWeixinwebOpenid() {
        return weixinwebOpenid;
    }

    /**
     * 设置web
     *
     * @param weixinwebOpenid web
     */
    public void setWeixinwebOpenid(String weixinwebOpenid) {
        this.weixinwebOpenid = weixinwebOpenid;
    }

    /**
     * @return qq_OpenId
     */
    public String getQqOpenid() {
        return qqOpenid;
    }

    /**
     * @param qqOpenid
     */
    public void setQqOpenid(String qqOpenid) {
        this.qqOpenid = qqOpenid;
    }

    /**
     * @return qq_UnionId
     */
    public String getQqUnionid() {
        return qqUnionid;
    }

    /**
     * @param qqUnionid
     */
    public void setQqUnionid(String qqUnionid) {
        this.qqUnionid = qqUnionid;
    }

    /**
     * 获取地址
     *
     * @return location - 地址
     */
    public String getLocation() {
        return location;
    }

    /**
     * 设置地址
     *
     * @param location 地址
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * 获取经验值
     *
     * @return experience - 经验值
     */
    public Integer getExperience() {
        return experience;
    }

    /**
     * 设置经验值
     *
     * @param experience 经验值
     */
    public void setExperience(Integer experience) {
        this.experience = experience;
    }

    /**
     * 获取VIP等级
     *
     * @return vip_Level - VIP等级
     */
    public Integer getVipLevel() {
        return vipLevel;
    }

    /**
     * 设置VIP等级
     *
     * @param vipLevel VIP等级
     */
    public void setVipLevel(Integer vipLevel) {
        this.vipLevel = vipLevel;
    }

    /**
     * 获取乐观锁版本控制
     *
     * @return Version - 乐观锁版本控制
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * 设置乐观锁版本控制
     *
     * @param version 乐观锁版本控制
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * 获取注册使用平台  0：UNKNOWN  1：ANDROID  2：IOS
     *
     * @return Reg_Platform - 注册使用平台  0：UNKNOWN  1：ANDROID  2：IOS
     */
    public Boolean getRegPlatform() {
        return regPlatform;
    }

    /**
     * 设置注册使用平台  0：UNKNOWN  1：ANDROID  2：IOS
     *
     * @param regPlatform 注册使用平台  0：UNKNOWN  1：ANDROID  2：IOS
     */
    public void setRegPlatform(Boolean regPlatform) {
        this.regPlatform = regPlatform;
    }

    /**
     * 获取注册设备标识
     *
     * @return Reg_IMEI - 注册设备标识
     */
    public String getRegImei() {
        return regImei;
    }

    /**
     * 设置注册设备标识
     *
     * @param regImei 注册设备标识
     */
    public void setRegImei(String regImei) {
        this.regImei = regImei;
    }

    /**
     * 获取最近使用平台 0：UNKNOWN 1：ANDROID 2：IOS
     *
     * @return Last_Platform - 最近使用平台 0：UNKNOWN 1：ANDROID 2：IOS
     */
    public Boolean getLastPlatform() {
        return lastPlatform;
    }

    /**
     * 设置最近使用平台 0：UNKNOWN 1：ANDROID 2：IOS
     *
     * @param lastPlatform 最近使用平台 0：UNKNOWN 1：ANDROID 2：IOS
     */
    public void setLastPlatform(Boolean lastPlatform) {
        this.lastPlatform = lastPlatform;
    }

    /**
     * 获取设备标识
     *
     * @return Last_IMEI - 设备标识
     */
    public String getLastImei() {
        return lastImei;
    }

    /**
     * 设置设备标识
     *
     * @param lastImei 设备标识
     */
    public void setLastImei(String lastImei) {
        this.lastImei = lastImei;
    }

    /**
     * 获取最后登录时间
     *
     * @return Last_Login - 最后登录时间
     */
    public Date getLastLogin() {
        return lastLogin;
    }

    /**
     * 设置最后登录时间
     *
     * @param lastLogin 最后登录时间
     */
    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    /**
     * 获取最后登录方式  0：未知 1：密码登录 2：验证码登录  3：免密登录 4：其它
     *
     * @return Last_Login_Type - 最后登录方式  0：未知 1：密码登录 2：验证码登录  3：免密登录 4：其它
     */
    public Boolean getLastLoginType() {
        return lastLoginType;
    }

    /**
     * 设置最后登录方式  0：未知 1：密码登录 2：验证码登录  3：免密登录 4：其它
     *
     * @param lastLoginType 最后登录方式  0：未知 1：密码登录 2：验证码登录  3：免密登录 4：其它
     */
    public void setLastLoginType(Boolean lastLoginType) {
        this.lastLoginType = lastLoginType;
    }

    /**
     * 获取累积登录次数
     *
     * @return Login_Times - 累积登录次数
     */
    public Integer getLoginTimes() {
        return loginTimes;
    }

    /**
     * 设置累积登录次数
     *
     * @param loginTimes 累积登录次数
     */
    public void setLoginTimes(Integer loginTimes) {
        this.loginTimes = loginTimes;
    }

    /**
     * 获取邀请者ID
     *
     * @return Inviter_Account_Id - 邀请者ID
     */
    public Long getInviterAccountId() {
        return inviterAccountId;
    }

    /**
     * 设置邀请者ID
     *
     * @param inviterAccountId 邀请者ID
     */
    public void setInviterAccountId(Long inviterAccountId) {
        this.inviterAccountId = inviterAccountId;
    }

    /**
     * 获取邀请绑定时间
     *
     * @return Invit_Time - 邀请绑定时间
     */
    public Date getInvitTime() {
        return invitTime;
    }

    /**
     * 设置邀请绑定时间
     *
     * @param invitTime 邀请绑定时间
     */
    public void setInvitTime(Date invitTime) {
        this.invitTime = invitTime;
    }

    /**
     * 获取邀请类型:0:用户正常邀请, 1:系统绑定
     *
     * @return Invit_Type - 邀请类型:0:用户正常邀请, 1:系统绑定
     */
    public Byte getInvitType() {
        return invitType;
    }

    /**
     * 设置邀请类型:0:用户正常邀请, 1:系统绑定
     *
     * @param invitType 邀请类型:0:用户正常邀请, 1:系统绑定
     */
    public void setInvitType(Byte invitType) {
        this.invitType = invitType;
    }

    /**
     * 获取是否已经实名认证
     *
     * @return Is_Real_Name_Auth - 是否已经实名认证
     */
    public Boolean getIsRealNameAuth() {
        return isRealNameAuth;
    }

    /**
     * 设置是否已经实名认证
     *
     * @param isRealNameAuth 是否已经实名认证
     */
    public void setIsRealNameAuth(Boolean isRealNameAuth) {
        this.isRealNameAuth = isRealNameAuth;
    }

    /**
     * 获取最后更换账户时间
     *
     * @return Last_Change_Time - 最后更换账户时间
     */
    public Date getLastChangeTime() {
        return lastChangeTime;
    }

    /**
     * 设置最后更换账户时间
     *
     * @param lastChangeTime 最后更换账户时间
     */
    public void setLastChangeTime(Date lastChangeTime) {
        this.lastChangeTime = lastChangeTime;
    }

    /**
     * 获取ip地址
     *
     * @return ip_address - ip地址
     */
    public String getIpAddress() {
        return ipAddress;
    }

    /**
     * 设置ip地址
     *
     * @param ipAddress ip地址
     */
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    /**
     * 获取ip归属地
     *
     * @return ip_location - ip归属地
     */
    public String getIpLocation() {
        return ipLocation;
    }

    /**
     * 设置ip归属地
     *
     * @param ipLocation ip归属地
     */
    public void setIpLocation(String ipLocation) {
        this.ipLocation = ipLocation;
    }

    /**
     * 获取备注
     *
     * @return comment - 备注
     */
    public String getComment() {
        return comment;
    }

    /**
     * 设置备注
     *
     * @param comment 备注
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * 获取创建时间
     *
     * @return Create_Time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取最后更新时间
     *
     * @return Last_Time - 最后更新时间
     */
    public Date getLastTime() {
        return lastTime;
    }

    /**
     * 设置最后更新时间
     *
     * @param lastTime 最后更新时间
     */
    public void setLastTime(Date lastTime) {
        this.lastTime = lastTime;
    }
}