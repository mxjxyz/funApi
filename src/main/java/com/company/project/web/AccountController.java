package com.company.project.web;

import com.alibaba.fastjson.JSONObject;
import com.company.project.core.Result;
import com.company.project.core.ResultGenerator;
import com.company.project.model.Account;
import com.company.project.pojo.PoAccount;
import com.company.project.service.AccountService;
import com.company.project.service.impl.RedisService;
import com.company.project.token.TokenUserDetails;
import com.company.project.token.TokenUserDetailsService;
import com.company.project.token.TokenUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.*;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

/**
 * Created by xyz on 2018/04/15.
 */
@RestController
@RequestMapping("/account")
@Api(description = "用户信息相关")
public class AccountController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(AccountController.class);

    @Resource
    private AccountService accountService;
    @Autowired
    RedisService redisService;

    @Autowired
    TokenUtil tokenUtil;
    @Autowired
    private TokenUserDetailsService tokenUserDetailsService;

    @ApiOperation(value = "用户登录", notes = "传用户名、密码和验证码，密码请传MD5后的，返回token", response = Result.class)
    @PostMapping("/login")
    public Result login(@RequestBody PoAccount account) {
        logger.debug("request body:" + JSONObject.toJSONString(account));
        TokenUserDetails user = (TokenUserDetails) tokenUserDetailsService.loadUserByUsername(account.getAccount());
        if (user.getUser() == null) {
            return ResultGenerator.genFailResult("账号不存在！");
        }
        if (!user.getUser().getPassword().equals(DigestUtils.md5Hex(account.getPassword()))) {
            return ResultGenerator.genFailResult("密码错误！");
        }
        String token = tokenUtil.generateToken(user);

        redisService.set(token, JSONObject.toJSONString(user.getUser()));

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("token", token);
        return ResultGenerator.genSuccessResult(jsonObject);
    }

    @ApiOperation(value = "用户基本信息", notes = "token", response = Result.class)
    @PostMapping("/auth/get")
    public Result get() {


        System.out.println("user get ....");
        return ResultGenerator.genSuccessResult("user get ");
    }

    @GetMapping("/auth/logout")
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate(); //使当前会话失效
        }
        SecurityContextHolder.clearContext(); //清空安全上下文
    }


    @GetMapping("/test")
    public Result test(@RequestParam String param) {
        System.out.println("test get ....");
        redisService.set("aaa", param);

        String adfd = redisService.get("aaa");
        return ResultGenerator.genSuccessResult(adfd);
    }


}
