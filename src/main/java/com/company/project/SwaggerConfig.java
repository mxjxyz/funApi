package com.company.project;

import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Created by f00lish on 2017/7/4.
 * Qun:530350843
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("v1")
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.company.project.web"))
                .paths(Predicates.or(
                        //这里添加你需要展示的接口
//                        PathSelectors.ant("/account/**"),
                        PathSelectors.ant("/account/**")
                        )
                )
                .build();

//        .paths(PathSelectors.any())
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Fun-API-APP-RESTful APIs")
                .description("欢迎使用---Fun-API-APP")
                .contact(new Contact("Fun","",""))
                .version("1.0")
                .build();
    }
}
