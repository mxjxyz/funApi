package com.company.project.token;


import com.company.project.model.Account;
import com.company.project.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

/**
 * Created by xcmg_HRQ on 2017/5/27.
 */
@Component
public class TokenUserDetailsService implements UserDetailsService {

    @Autowired
    private AccountService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Account userInfo = userService.getUserByUsername(username);
        TokenUserDetails userDetails = new TokenUserDetails(userInfo);
        if (userDetails == null) {
            throw new UsernameNotFoundException(username);
        }
        return userDetails;
    }
}
