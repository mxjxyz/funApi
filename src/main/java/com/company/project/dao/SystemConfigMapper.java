package com.company.project.dao;

import com.company.project.core.Mapper;
import com.company.project.model.SystemConfig;

public interface SystemConfigMapper extends Mapper<SystemConfig> {
}